<?php

$MESS["ADDITION_PRODUCTS_UPDATE_TEXT_CNT_PL"] = '<p>Поле <b>Объем</b> изменено с <b>#BEFORE_DATA# пал.</b> на <b>#AFTER_DATA# пал.</b></p>';
$MESS["ADDITION_PRODUCTS_UPDATE_TEXT_CNT_PL_E"] = '<p>Поле <b>Объем</b> изменено на <b>#AFTER_DATA# пал.</b></p>';
$MESS["ADDITION_PRODUCTS_UPDATE_TEXT_CNT_UT"] = '<p>Поле <b>Объем</b> изменено с <b>#BEFORE_DATA# шт.</b> на <b>#AFTER_DATA# шт.</b></p>';
$MESS["ADDITION_PRODUCTS_UPDATE_TEXT_CNT_UT_E"] = '<p>Поле <b>Объем</b> изменено на <b>#AFTER_DATA# шт.</b></p>';
$MESS["ADDITION_PRODUCTS_UPDATE_TEXT_CNT_TN"] = '<p>Поле <b>Объем</b> изменено с <b>#BEFORE_DATA# т.</b> на <b>#AFTER_DATA# т.</b></p>';
$MESS["ADDITION_PRODUCTS_UPDATE_TEXT_CNT_TN_E"] = '<p>Поле <b>Объем</b> изменено на <b>#AFTER_DATA# т.</b></p>';
$MESS["ADDITION_PRODUCTS_UPDATE_TEXT_PRICE_AGREEMENT"] = '<p>Поле <b>Прайс</b> изменено с <b>#BEFORE_DATA#</b> на <b>#AFTER_DATA#</b></p>';
$MESS["ADDITION_PRODUCTS_UPDATE_TEXT_PRICE_AGREEMENT_E"] = '<p>Поле <b>Прайс</b> изменено на <b>#AFTER_DATA#</b></p>';
$MESS["ADDITION_PRODUCTS_UPDATE_TEXT_PRICE_TYPE"] = '<p>Поле <b>Вид цены</b> изменено с <b>#BEFORE_DATA#</b> на <b>#AFTER_DATA#</b></p>';
$MESS["ADDITION_PRODUCTS_UPDATE_TEXT_PRICE_TYPE_E"] = '<p>Поле <b>Вид цены</b> изменено на <b>#AFTER_DATA#</b></p>';
$MESS["ADDITION_PRODUCTS_UPDATE_TEXT_PRICE"] = '<p>Поле <b>Цена без НДС</b> изменено с <b>#BEFORE_DATA# руб.</b> на <b>#AFTER_DATA# руб.</b></p>';
$MESS["ADDITION_PRODUCTS_UPDATE_TEXT_PRICE_E"] = '<p>Поле <b>Цена без НДС</b> изменено на <b>#AFTER_DATA# руб.</b></p>';
$MESS["ADDITION_PRODUCTS_UPDATE_TEXT_SPECIAL_PRICE"] = '<p>Снят флаг "Спец. цена"</p>';
$MESS["ADDITION_PRODUCTS_UPDATE_TEXT_SPECIAL_PRICE_E"] = '<p>Установлен флаг "Спец. цена"</p>';
