<?php
$korus_basic_default_option = [
    'auth_user_create_email_to_send' => '',
    'diadoc_link' => 'https://auth.kontur.ru/?tabs=1,1,0,1&customize=diadoc&back=https%3A%2F%2Fdiadoc.kontur.ru%2F7a016e0c-5459-4ec4-94b8-07ddcbd85ad0%2FCounteragents',
    'template_request' => '/upload/demo/template_request.xlsx',
    'template_request_spec' => '/upload/demo/template_request_special_price.xlsx',
    'template_group_rules' => '/upload/demo/Assortment.xlsx',
    'template_nonpallet_products_rules' => '/upload/demo/NonpalletProducts.xlsx',
    'template_order' => '/upload/demo/template_order.xlsx',
];
