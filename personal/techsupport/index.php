<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Форма технической поддержки");
$APPLICATION->AddChainItem($APPLICATION->GetTitle());
?>
    <div style="padding: 2.2rem;">
        <div class="content-body">
            <?
            $APPLICATION->IncludeComponent(
                "bitrix:support.ticket",
                "techsupport",
                [
                    "MESSAGES_PER_PAGE"   => "20",   // Количество сообщений на одной странице
                    "MESSAGE_MAX_LENGTH"  => "70",   // Максимальная длина неразрывной строки
                    "MESSAGE_SORT_ORDER"  => "asc",  // Направление для сортировки сообщений в обращении
                    "SEF_MODE"            => "N",    // Включить поддержку ЧПУ
                    "SET_PAGE_TITLE"      => "Y",    // Устанавливать заголовок страницы
                    "SET_SHOW_USER_FIELD" => [       // Связь с кастомным полем
                                                     0 => "UF_CONTRACT",
                    ],                               // Показывать пользовательские поля
                    "SHOW_COUPON_FIELD"   => "N",    // Показывать поле ввода купона
                    "TICKETS_PER_PAGE"    => "50",   // Количество обращений на одной странице
                    "COMPONENT_TEMPLATE"  => "techsupport",
                    "VARIABLE_ALIASES"    => [
                        "ID" => "ID",
                    ],
                ],
                false
            ); ?>
        </div>
    </div>
    <!--    <div id="panel">
        <?
    /* $APPLICATION->ShowPanel(); */ ?>
    </div>-->
<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>