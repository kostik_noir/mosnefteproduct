<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
/** @global \CMain $APPLICATION */
$APPLICATION->SetTitle("Восстановление пароля");
/** @global \CUser $USER */
//if ($USER->IsAuthorized()) {
//    LocalRedirect('/');
//}
$APPLICATION->IncludeComponent(
    "bitrix:system.auth.changepasswd",
    "flat",
    [
        "SHOW_ERRORS" => "Y",
        'AUTH_RESULT' => $APPLICATION->arAuthResult
    ]
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
